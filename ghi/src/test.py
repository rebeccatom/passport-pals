def missing_integer(arr):
    arr.sort()

    ans = 1
    for c in arr:
        if c > 0 and c > ans:
            return ans
        elif c > 0 and c <= ans:
            ans += 1
    return ans

print(missing_integer([1,2,4]))
