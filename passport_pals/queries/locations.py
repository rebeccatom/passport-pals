def get_locations():
    locations = [
        {"value": "San Diego, CA, USA", "label": "San Diego, CA USA"},
        {"value": "Paris, France", "label": "Paris, France"},
        {"value": "San Juan, Puerto Rico", "label": "San Juan, PR"},
        {"value": "Auckland, NZ", "label": "Auckland, NZ"},
        {"value": "New York City, NY, USA", "label": "New York City, NY, USA"},
        {"value": "Barcelona, Spain", "label": "Barcelona, Spain"},
    ]
    return locations
